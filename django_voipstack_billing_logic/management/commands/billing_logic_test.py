import phonenumbers
from django.core.management.base import BaseCommand
from phonenumbers import PhoneNumberType


class Command(BaseCommand):
    def handle(self, *args, **options):
        phone_number = phonenumbers.parse("+49219285490", None)
        phone_number_type = phonenumbers.number_type(phone_number)

        print(phone_number_type)

        if phone_number_type == PhoneNumberType.MOBILE:
            print("MOBILE")

        if phone_number_type == PhoneNumberType.FIXED_LINE:
            print("FIXED_LINE")

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
